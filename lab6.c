//#define F_CPU 16000000UL   // Not necessary in simulator
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include<stdio.h> 
#include<stdlib.h>
#include<string.h>
#define _XOPEN_SOURCE_EXTENDED 1


// Defination of pot and pin variables
#define LCD_Port         PORTD	// Define LCD Port (PORTA, PORTB, PORTC, PORTD)
#define LCD_DPin         DDRD	// Define 4-Bit Pins (PD4-PD7 at PORT D)
#define LCD_DATA0_PIN    PD4    // < pin for 4bit data bit 0  
#define LCD_DATA1_PIN    PD5    // < pin for 4bit data bit 1  
#define LCD_DATA2_PIN    PD6    // < pin for 4bit data bit 2  
#define LCD_DATA3_PIN    PD7    // < pin for 4bit data bit 3  
#define RSPIN 			 PD3	// RS Pin
#define ENPIN            PD2 	// E Pin
int runtime;			 //Timer for LCD

// LCD initialization function with the minimum commands list
void LCD_Init (void)
{
    // Data and control pins as outputs
    LCD_DPin |= (1<<LCD_DATA0_PIN)|(1<<LCD_DATA1_PIN)|(1<<LCD_DATA2_PIN)|(1<<LCD_DATA3_PIN)|(1<<RSPIN)|(1<<ENPIN);		//Control LCD Pins (D4-D7)
	_delay_ms(16);		//Wait more when 15 ms after start
  
  
	LCD_Action(0x02);	    // Returns the cursor to the home position (Address 0). Returns display to its original state if it was shifted. 
	LCD_Action(0x28);       // Data sent or received in 4 bit lengths (DB7-DB4)
	LCD_Action(0x0C);       // Display on, cursor off
	LCD_Action(0x06);       // Increment cursor (shift cursor to right)
	LCD_Action(0x01);       // Clean LCD
	_delay_ms(5);
}

// Function to send commands and data to the LCD
void LCD_Action( unsigned char cmnd )
{
	LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0);
	LCD_Port &= ~ (1<<RSPIN);
	LCD_Port |= (1<<ENPIN);
	_delay_us(1);
	LCD_Port &= ~ (1<<ENPIN);
	_delay_us(200);
	LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4);
	LCD_Port |= (1<<ENPIN);
	_delay_us(1);
	LCD_Port &= ~ (1<<ENPIN);
	_delay_ms(2);
}

// Clear whole LCD
void LCD_Clear()
{
	LCD_Action (0x01);		//Clear LCD
	_delay_ms(2);			//Wait to clean LCD
	LCD_Action (0x80);		//Move to Position Line 1, Position 1
}

// Prints string of chars 
void LCD_Print (char *str)
{
	int i;
	for(i=0; str[i]!=0; i++)
	{
		LCD_Port = (LCD_Port & 0x0F) | (str[i] & 0xF0);
		LCD_Port |= (1<<RSPIN);
		LCD_Port|= (1<<ENPIN);
		_delay_us(1);
		LCD_Port &= ~ (1<<ENPIN);
		_delay_us(200);
		LCD_Port = (LCD_Port & 0x0F) | (str[i] << 4);
		LCD_Port |= (1<<ENPIN);
		_delay_us(1);
		LCD_Port &= ~ (1<<ENPIN);
		_delay_ms(2);
	}
}

// Prints string of chars to the specific location
// row - the row 0 or 1;
// pos - the column from 0 till 16
void LCD_Printpos (char row, char pos, char *str)
{
	if (row == 0 && pos<16)
	LCD_Action((pos & 0x0F)|0x80);
	else if (row == 1 && pos<16)
	LCD_Action((pos & 0x0F)|0xC0);
	LCD_Print(str);
}


void InitADC()
{
 ADMUX |= (1<<ADLAR)|(1<<REFS0);
 ADCSRA |= (1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0)|(1<<ADEN);
}

uint16_t ReadADC(uint8_t ADCchannel){
 ADMUX = (ADMUX & 0xF0) | (ADCchannel & 0x0F);
 ADCSRA |= (1<<ADSC);
 while( ADCSRA & (1<<ADSC) );
 return ADCH*4+ADCL/64;
}


int main()
{

    	DDRB=0b00111000;
 	PCICR|=(1<<PCIE0);
  	PCMSK0|=(1<<PCINT0)|(1<<PCINT1)|(1<<PCINT2);
  	LCD_Init();
    Serial.begin(9600);
  
  	char temp_c[26];
  	String temp_s;
  	char vilniusmenu[17]={"VilniusTech Menu"};
  	char Temp1[17]={"Temp1: "};
  	char degrees[17]={"oC"};
  	InitADC();
    double temp;
  
	while(1) {
 		temp = (double)ReadADC(0x00) / 1024;
  		temp=((temp*5)-0.5)*100;                                  
    	temp_s=String(temp,DEC);
    	strcpy(temp_c,temp_s.c_str());
      
        LCD_Clear();
    	LCD_Printpos(0,0,vilniusmenu);
    	LCD_Printpos(1,0,Temp1);
    	LCD_Printpos(1,9,temp_c);
    	LCD_Printpos(1,14,degrees);     
    	_delay_ms(200);
	}
}